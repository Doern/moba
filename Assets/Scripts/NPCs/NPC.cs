﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

namespace MOBA.NPC
{
    public class NPC : MonoBehaviour, INPC
    {
        public string characterName;
        public List<Quest> _quests;
        public List<string> _dialogs;

        public Text dialogTextBox;
        public Button nextDialogButton;

        public GameObject hintedArea;

        bool _hasDialog;
        bool _hasQuest;

        bool _showDialog = false;

        int _currentDialogIndex = 0;

        public GameObject currentPlayerInDialog;

        void Start ( )
        {
            if ( _hasDialog )
            {
                dialogTextBox.gameObject.SetActive ( false );
                nextDialogButton.gameObject.SetActive ( false );
            }
        }

        void Update ( )
        {
            if ( _showDialog )
            {
                dialogTextBox.gameObject.SetActive ( true );
                nextDialogButton.gameObject.SetActive ( true );
                dialogTextBox.text = _dialogs [ _currentDialogIndex ].Replace ( "$NAME$", characterName );
            }
            else
            {
                dialogTextBox.gameObject.SetActive ( false );
                nextDialogButton.gameObject.SetActive ( false );
            }
        }

        public bool FirstTimeDialog ( )
        {
            throw new NotImplementedException ( );
        }

        public string GetDialog ( )
        {
            throw new NotImplementedException ( );
        }

        public bool GetHasDialog ( )
        {
            return ( _dialogs != null && _dialogs.Count > 0 );
        }

        public bool GetHasQuest ( )
        {
            return ( _quests != null && _quests.Count > 0 );
        }

        public void SetHasQuest ( bool b )
        {
            _hasQuest = b;
        }

        public void StartDialog ( )
        {
            if ( _hasDialog )
            {
                _showDialog = true;
            }
        }

        public void HideDialog ( )
        {
            _showDialog = false;
        }

        public void SetHasDialog ( bool b )
        {
            _hasDialog = b;
        }

        public void NextDialog ( )
        {
            if ( _currentDialogIndex < _dialogs.Count - 1 )
            {
                _currentDialogIndex++;
            }
            else
            {
                currentPlayerInDialog.GetComponentInParent<Click2Move> ( ).inDialog = false;
                HideDialog ( );
            }

            if ( _currentDialogIndex == 1 )
            {
                hintedArea.gameObject.SetActive ( true );
                nextDialogButton.GetComponentInChildren<Text> ( ).text = "OK";
            }
        }    
    }
}
