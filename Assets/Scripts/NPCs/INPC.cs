﻿namespace MOBA.NPC
{
    public interface INPC
    {
        void StartDialog ( );
        void HideDialog ( );
        bool FirstTimeDialog ( );

        bool GetHasQuest ( );
        void SetHasQuest ( bool b );

        bool GetHasDialog ( );
        string GetDialog ( );
        void SetHasDialog ( bool b );
    }
}