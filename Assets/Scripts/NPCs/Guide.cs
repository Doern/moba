﻿using UnityEngine;
using System.Collections;

namespace MOBA.NPC
{
    public class Guide : NPC
    {
        public bool hasDialog;
        public bool hasQuest;

        void Awake ( )
        {
            SetHasDialog ( hasDialog );
            SetHasQuest ( hasQuest );
        }

        void OnTriggerEnter ( Collider c )
        {
            if ( c.tag == "Player" )
            {
                currentPlayerInDialog = c.gameObject;
                c.gameObject.GetComponentInParent<Click2Move> ( ).inDialog = true;
                StartDialog ( );
            }
        }

        void OnTriggerExit ( Collider c )
        {
            if ( c.tag == "Player" )
            {
                currentPlayerInDialog = null;
                c.gameObject.GetComponentInParent<Click2Move> ( ).inDialog = false;
                HideDialog ( );
            }
        }
    }
}
