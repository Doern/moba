﻿using UnityEngine;
using System.Collections;

public class Click2Move : MonoBehaviour
{
    public GameObject graphics;

    public float speed = 5;

    [Range(0,2)]
    public int mouseButton2Move = 1;

    public float walkRange = .5f;

    public bool inDialog = false;

    Vector3 _newPosition;

    // Use this for initialization
    void Start ( )
    {
        _newPosition = graphics.transform.position;
    }

    // Update is called once per frame
    void Update ( )
    {
        bool RMB = Input.GetMouseButtonDown ( mouseButton2Move );

        if ( RMB && !inDialog )
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay ( Input.mousePosition );

            if ( Physics.Raycast ( ray, out hit ) )
            {
                if ( hit.transform.tag == "Ground" || hit.transform.tag == "NPC" )
                {
                    _newPosition = hit.point;

                    _newPosition = new Vector3 ( _newPosition.x, -3f, _newPosition.z );
                }
            }
        }

        if ( Vector3.Distance ( transform.position, _newPosition ) > walkRange )
        {
            transform.position = Vector3.MoveTowards ( transform.position, _newPosition, speed * Time.deltaTime );

            Quaternion transRot = Quaternion.LookRotation ( _newPosition - transform.position, Vector3.up );

            graphics.transform.rotation = Quaternion.Slerp ( transRot, graphics.transform.rotation, .7f );
        }
    }
}
